export interface Userinfo{
    UserName?: string;
    FirstName?: string;
    LastName?: string; 
    Address?:string;
    Birthday?: string;
    Gender?: string;
    PhoneNO?: string;
    Email?: string;
    Facebook?:string;
    Location?: string;
    AvgPoint?: number;
    Description?: string;
}
