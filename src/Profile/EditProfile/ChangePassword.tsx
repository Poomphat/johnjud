import React, { useEffect, useState } from 'react';
import '../Profile.css';
import ProfileService from '../ProfileService';
import profileservice from '../ProfileService';
import { Link } from 'react-router-dom';
import ProfilePic from '../ProfilePic.png';
import Glasspic from '../Glasspic.jpg';

const ChangePassword=() => {
    return(
        <div className = 'ChangePage'>
            <Link to='/home/profile/prodescribtion'>  
                <img id='profilePic' src={ProfilePic}></img>
            </Link>
            <img id='glasspic' src = {Glasspic}></img>
            <div className='BlockBehindProfilePic'>
                <div className='profilename'>
                <br/><br/>
                    <h1><u> username </u></h1>
                </div>
            </div>
            <div className='ChangeBlock'>
                <span id='ChangePassword'>Old Password*: </span>
                &nbsp;&nbsp;<input id='InputChangePassword'></input>
                <br/><br/>
                <span id='ChangePassword'>New Password*: </span>
                <input id='InputChangePassword'></input>
                <br/><br/>
                <span id='ChangePassword'>Confirm new Password*: </span>
                <input id='InputChangePassword'></input>
                <button id='SubmitPasswordButton'>Submit</button>
            </div>

        </div>
    )
}
export default ChangePassword;