import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import AppProfile from './Profile/AppProfile'

ReactDOM.render(
  <React.StrictMode>
    <AppProfile/>
  </React.StrictMode>,
  document.getElementById('root')
);
//
 // <TabProfile/>
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
